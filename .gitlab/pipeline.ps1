param (
    [string]$platform = "G2.0",
    [Parameter(Mandatory=$true)][string]$target,
    [string]$arch = "x86",
    [string]$sensor = "gen2",
    [string]$configuration = "TLS"
 )

write-output @"
stages:
  - sanity

.configuration:
  variables:
    TARGET: $target
    PLATFORM: $platform
    ARCH: $arch
  tags:
    - windows

"@

write-output @'
sanity:
  stage: sanity
  script:
    - dir
  extends:
    - .configuration
  rules:
    - if: $CI_COMMIT_BRANCH
'@    

